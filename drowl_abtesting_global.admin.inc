<?php

/**
 * @file
 * JQuery jQuery UI MultiSelect Widget admin/configuration functionality.
 */

/**
 * drowl_abtesting_global_settings_form.admin settings form.
 */
function drowl_abtesting_global_settings_form() {
  $form[DROWL_ABTESTING_GLOBAL_SETTING_CODE] = array(
    '#type' => 'textfield',
    '#title' => t('Google Analytics Experiments Test Key'),
    '#description' => t("The Google Analytics Experiments Test Key. Format for example (XXXXXXXXX-X) Add a test in Google Analytics > Behaviours > Tests > Add and past the test key (NOT Test-ID) here. See !link.", array('!link' => l('https://support.google.com/analytics/answer/1745216', 'https://support.google.com/analytics/answer/1745216'))),
    '#required' => TRUE,
    '#default_value' => variable_get(DROWL_ABTESTING_GLOBAL_SETTING_CODE, ''));

  $form[DROWL_ABTESTING_GLOBAL_SETTING_STORE] = array(
    '#type' => 'checkbox',
    '#title' => t('Store variation in session'),
    '#description' => t('Store the first variation set in the session for further calls. This is normally not required because Google Analytics serves the same cev for every call.'),
    '#required' => FALSE,
    '#default_value' => variable_get(DROWL_ABTESTING_GLOBAL_SETTING_STORE, FALSE));

  $form['role_vis_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Role specific script settings'),
    '#collapsible' => TRUE,
    '#group' => 'zopim',
  );

  $roles = user_roles();
  $role_options = array();
  foreach ($roles as $rid => $name) {
    $role_options[$rid] = $name;
  }
  $form['role_vis_settings'][DROWL_ABTESTING_GLOBAL_SETTING_ROLES] = array(
    '#type' => 'checkboxes',
    '#title' => t('Remove script for specific roles'),
    '#default_value' => variable_get(DROWL_ABTESTING_GLOBAL_SETTING_ROLES, array()),
    '#options' => $role_options,
    '#description' => t('Remove script only for the selected role(s). If none of the roles are selected, all roles will have the script. Otherwise, any roles selected here will NOT have the script.'),
  );

  // Page specific visibility configurations.
  $form['page_vis_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page specific script settings'),
    '#collapsible' => TRUE,
    '#group' => 'drowl_abtesting_global',
  );

  $access = user_access('use PHP for content experiments visibility');
  $visibility = variable_get(DROWL_ABTESTING_GLOBAL_SETTING_GLOBAL_VISIBILITY, 0);
  $pages = variable_get(DROWL_ABTESTING_GLOBAL_SETTING_PAGES, '');

  if ($visibility == 2 && !$access) {
    $form['page_vis_settings'] = array();
    $form['page_vis_settings']['visibility'] = array('#type' => 'value', '#value' => 2);
    $form['page_vis_settings']['pages'] = array('#type' => 'value', '#value' => $pages);
  }
  else {
    $options = array(t('Add to every page except the listed pages.'), t('Add to the listed pages only.'));
    $description = t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));
    if ($access) {
      $options[] = t('Add if the following PHP code returns <code>TRUE</code> (PHP-mode, experts only).');
      $description .= ' ' . t('If the PHP-mode is chosen, enter PHP code between %php tags. Note that executing incorrect PHP-code can break your Drupal site.', array('%php' => '<?php ?>'));
    }
    $form['page_vis_settings']['drowl_abtesting_global_visibility'] = array(
      '#type' => 'radios',
      '#title' => t('Add script to specific pages'),
      '#options' => $options,
      '#default_value' => $visibility,
    );
    $form['page_vis_settings']['drowl_abtesting_global_pages'] = array(
      '#type' => 'textarea',
      '#title' => t('Pages'),
      '#default_value' => $pages,
      '#description' => $description,
      '#wysiwyg' => FALSE,
    );
  }

  return system_settings_form($form);
}