Module to execute global (in most cases design / theme) A/B-Testings. 
This module does not execute actions itself but provides a hook (see drowl_abtesting_global.api.php) to execute them
based on the currently active variation.

Guide how to create Google Analytics content experiments for multiple pages:
http://www.hallme.com/blog/running-google-content-experiment-multiple-pages/

Google Analytics Experiments manual:
https://support.google.com/analytics/answer/1745216

Special thanks to the maintainer of the content_experiments module which this module was inspired of.