<?php

/**
 * @file HOOKs examples for drowl_abtesting_global.
 */

/**
 * Implements HOOK_drowl_abtesting_global_handle_variation.
 */
function HOOK_drowl_abtesting_global_handle_variation($parameter) {
  // Define the concrete variation reactions.
  switch ($parameter) {
    case 1:
      // Case parameter = 1
      // Activate context
      // $context = context_load('drowl_abtesting_global_variation1');
      // context_set('context', $context->name, $context);
      break;

    case 2:
      // Case parameter = 2
      // Add specific CSS file
      // drupal_add_css(...);
      break;

    case 'abc':
      // Case parameter = 'abc'
      break;

    default:
      // Default / Fallback
      break;
  }
}

/**
 * Implements HOOK_drowl_abtesting_global_process_variation_alter
 */
function HOOK_drowl_abtesting_global_process_variation_alter(&$variation_identifier) {
  // You may manipulate the variation identifier here.
}
